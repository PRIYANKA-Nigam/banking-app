-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2023 at 11:48 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `name` varchar(255) NOT NULL,
  `Mob` varchar(100) NOT NULL,
  `Bank` varchar(255) NOT NULL,
  `code` int(30) NOT NULL,
  `Date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT DELAYED INTO `admin` (`name`, `Mob`, `Bank`, `code`, `Date`) VALUES
('ashi', '9450728740', 'Bank of Baroda', 12902, '2023-03-26'),
('xyz', '1234567890', 'Axis Bank', 125902, '2023-04-25'),
('sheenu', '918932946515', 'Punjab National Bank', 15658902, '2023-03-27'),
('chiya', '+918932946515', 'HDFC Bank', 16786456, '2023-03-26'),
('PRIYANKA_Nigam', '917701868611', 'Federal Bank', 124983902, '2023-03-27'),
('ppy', '1234567890', 'State Bank of India', 129675902, '2023-04-25'),
('cheenu', '918932946515', 'Yes Bank', 179977902, '2023-03-27'),
('priyanka', '9129520224', 'UCO Bank', 2147483647, '2023-03-26');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `name` varchar(255) NOT NULL,
  `mob` varchar(100) NOT NULL,
  `Bank` varchar(255) NOT NULL,
  `acc_num` int(100) NOT NULL,
  `Balance` int(255) NOT NULL,
  `mode` varchar(255) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT DELAYED INTO `user` (`name`, `mob`, `Bank`, `acc_num`, `Balance`, `mode`, `date`) VALUES
('tina', '9450728740', 'IDBI Bank', 134219, 2602, 'Deposit', '2023-03-26'),
('hina', '9129520224', 'HDFC Bank', 145727, 3600, 'Withdraw', '2023-03-26'),
('seema', '8317063305', 'Federal Bank', 152902, 4000, '', '2023-03-30'),
('chiya', '7701868611', 'Citibank', 153392, 3412, 'Withdraw', '2023-03-26'),
('chiya', '5678902345', 'Bank of Baroda', 174563, 3750, 'Withdraw', '2023-03-26'),
('abc', '12223333440', 'Axis Bank', 1028902, 3000, '', '2023-04-25'),
('pikachu', '5678902345', 'Federal Bank', 1317902, 8500, 'Deposit', '2023-03-27'),
('aac', '7701868611', 'State Bank of India', 10340902, 6500, 'Withdraw', '2023-04-25'),
('priya', '7701868611', 'UCO Bank', 14697576, 5682, 'Deposit', '2023-03-26'),
('piku', '9129520224', 'ICICI Bank', 145304990, 3456, '', '2023-03-26'),
('pixie', '8317063303', 'Bank of Baroda', 161127190, 3000, '', '2023-03-26'),
('meera', '8317063303', 'Federal Bank', 188766902, 3600, 'Deposit', '2023-03-27'),
('aab', '9129520224', 'State Bank of India', 2147483647, 2000, '', '2023-04-25');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role` enum('user','admin') NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT DELAYED INTO `users` (`id`, `role`, `username`, `password`, `name`) VALUES
(2, 'admin', 'priya', '81dc9bdb52d04dc20036dbd8313ed055', 'priya n'),
(3, 'user', 'chiya', 'e2fc714c4727ee9395f324cd2e7f331f', 'chiya n');

-- --------------------------------------------------------

--
-- Table structure for table `user_history`
--

CREATE TABLE `user_history` (
  `acc_num` varchar(255) NOT NULL,
  `Balance` int(255) NOT NULL,
  `Amount` int(255) NOT NULL,
  `Mode` varchar(255) NOT NULL,
  `Date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_history`
--

INSERT DELAYED INTO `user_history` (`acc_num`, `Balance`, `Amount`, `Mode`, `Date`) VALUES
('2147483647', 3425, 0, '', '2023-03-27'),
('2147483647', 3425, 2000, '', '2023-03-27'),
('2147483647', 1425, 2000, 'Withdraw', '2023-03-27'),
('2147483647', -575, 10000, 'Withdraw', '2023-03-27'),
('2147483647', 9425, 0, 'Deposit', '2023-03-27'),
('2147483647', 9425, 10000, 'Deposit', '2023-03-27'),
('2147483647', 19425, 10000, 'Deposit', '2023-03-27'),
('2147483647', 29425, 2000, 'Deposit', '2023-03-27'),
('161127190', 2380, 0, 'Deposit', '2023-03-28'),
('161127190', 2380, 20, 'Deposit', '2023-03-28'),
('161127190', 2400, 0, 'Deposit', '2023-03-28'),
('161127190', 2400, 0, 'Deposit', '2023-03-28'),
('161127190', 2400, 400, 'Deposit', '2023-03-28'),
('161127190', 2000, 0, 'Withdraw', '2023-03-28'),
('161127190', 2000, 0, 'Withdraw', '2023-03-28'),
('161127190', 2000, 0, 'Withdraw', '2023-03-28'),
('161127190', 2000, 2000, 'Withdraw', '2023-03-28'),
('161127190', 0, 2500, '', '2023-03-28'),
('161127190', 2500, 0, 'Deposit', '2023-03-28'),
('161127190', 2500, 500, '', '2023-03-28'),
('161127190', 2000, 0, 'ICICI Bank', '2023-03-28'),
('161127190', 2000, 2000, '', '2023-03-28'),
('161127190', 4000, 0, 'Deposit', '2023-03-28'),
('161127190', 4000, 0, '', '2023-03-28'),
('161127190', 4000, 1000, '', '2023-03-28'),
('161127190', 3000, 0, 'Withdraw', '2023-03-28'),
('161127190', 3000, 0, '', '2023-03-28'),
('161127190', 3000, 0, '', '2023-03-28'),
('161127190', 3000, 0, '', '2023-03-28'),
('161127190', 3000, 0, '', '2023-03-28'),
('161127190', 3000, 0, '', '2023-03-28'),
('161127190', 3000, 0, '', '2023-03-28'),
('161127190', 3000, 0, '', '2023-03-28'),
('161127190', 3000, 0, '', '2023-03-28'),
('161127190', 3000, 0, '', '2023-03-28'),
('161127190', 3000, 0, '', '2023-03-28'),
('145727', 3056, 0, 'Withdraw', '2023-03-28'),
('145727', 3056, 44, '', '2023-03-28'),
('145727', 3100, 0, 'Deposit', '2023-03-28'),
('145727', 3100, 400, '', '2023-03-28'),
('145727', 3500, 0, 'Deposit', '2023-03-28'),
('145727', 3500, 500, '', '2023-03-28'),
('145727', 4000, 0, 'Deposit', '2023-03-28'),
('145727', 4000, 1000, '', '2023-03-28'),
('145727', 3000, 0, 'Withdraw', '2023-03-28'),
('145727', 3000, 0, '', '2023-03-28'),
('145727', 3000, 1000, '', '2023-03-28'),
('145727', 4000, 0, 'Deposit', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 0, '', '2023-03-28'),
('145727', 4000, 500, '', '2023-03-28'),
('145727', 3500, 0, 'Withdraw', '2023-03-28'),
('145727', 3500, 0, 'Withdraw', '2023-03-28'),
('145727', 3500, 0, 'Withdraw', '2023-03-28'),
('145727', 3500, 500, 'Withdraw', '2023-03-28'),
('145727', 4000, 0, 'Deposit', '2023-03-28'),
('145727', 4000, 500, 'Deposit', '2023-03-28'),
('145727', 4500, 0, 'Deposit', '2023-03-28'),
('145727', 4500, 100, 'Deposit', '2023-03-28'),
('145727', 4400, 0, 'Withdraw', '2023-03-28'),
('145727', 4400, 200, 'Withdraw', '2023-03-28'),
('145727', 4600, 0, 'Deposit', '2023-03-28'),
('1317902', 2500, 0, '', '2023-03-28'),
('1317902', 2500, 1000, '', '2023-03-28'),
('1317902', 2500, 0, '', '2023-03-28'),
('1317902', 2500, 0, '', '2023-03-28'),
('1317902', 2500, 1000, '', '2023-03-28'),
('1317902', 3500, 0, 'Deposit', '2023-03-28'),
('188766902', 3567, 0, '', '2023-03-30'),
('188766902', 3567, 0, '', '2023-03-30'),
('188766902', 3567, 33, '', '2023-03-30'),
('188766902', 3600, 0, 'Deposit', '2023-03-30'),
('188766902', 3600, 0, 'Deposit', '2023-03-30'),
('1317902', 3500, 0, 'Deposit', '2023-04-25'),
('1317902', 3500, 5000, 'Deposit', '2023-04-25'),
('1317902', 8500, 0, 'Deposit', '2023-04-25'),
('145727', 4600, 0, 'Deposit', '2023-04-25'),
('145727', 4600, 1000, 'Deposit', '2023-04-25'),
('145727', 3600, 0, 'Withdraw', '2023-04-25'),
('1317902', 8500, 0, 'Deposit', '2023-04-25'),
('10340902', 3500, 0, '', '2023-04-25'),
('10340902', 3500, 4000, '', '2023-04-25'),
('10340902', 7500, 0, 'Deposit', '2023-04-25'),
('10340902', 7500, 1000, 'Deposit', '2023-04-25'),
('10340902', 6500, 0, 'Withdraw', '2023-04-25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`code`),
  ADD UNIQUE KEY `name` (`name`,`Mob`),
  ADD UNIQUE KEY `Bank` (`Bank`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`acc_num`),
  ADD UNIQUE KEY `name` (`name`,`mob`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
